namespace FileSharer.Common.Entities
{
    public class FileCategory
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
    }
}
