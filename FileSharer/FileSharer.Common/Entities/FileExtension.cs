namespace FileSharer.Common.Entities
{
    public class FileExtension
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
