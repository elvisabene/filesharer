﻿namespace FileSharer.Common.Entities
{
    public class PhysicalFile
    {
        public byte[] Data { get; set; }

        public string Name { get; set; }

        public string ContentType { get; set; }
    }
}
