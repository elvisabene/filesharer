﻿using System;

namespace FileSharer.Common.Helpers
{
    public static class Guard
    {
        public static void EnsureNotNull(object argument, string argumentName)
        {
            TryCatchThrow(() =>
            {
                if (argument is null)
                {
                    throw new ArgumentNullException(argumentName);
                }
            });
        }

        public static void EnsurePresenceOfString(string argument, string argumentName)
        {
            TryCatchThrow(() =>
            {
                if (string.IsNullOrEmpty(argument))
                {
                    throw new ArgumentNullException(argument, $"The specified '{argumentName}' was null or empty.");
                }
            });
        }

        public static void EnsurePositive(int value, string valueName)
        {
            TryCatchThrow(() =>
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException(valueName, $"The specified '{valueName}' was not positive.");
                }
            });
        }

        private static void TryCatchThrow(Action action)
        {
            try
            {
                action?.Invoke();
            }
            catch
            {
                throw;
            }
        }
    }
}
