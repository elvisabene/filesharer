﻿using System;

namespace FileSharer.Common.Constants
{
    public static class Paths
    {
        public static string LocalStorageDirectory = Environment.CurrentDirectory;
    }
}
