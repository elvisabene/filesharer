﻿namespace FileSharer.Common.Constants
{
    public static class Roles
    {
        public const string Guest = "Guest";

        public const string User = "User";

        public const string Editor = "Editor";

        public const string Admin = "Admin";

        public const string OnlyEditors = "Admin, Editor";
    }
}
