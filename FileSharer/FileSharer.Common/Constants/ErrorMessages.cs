﻿namespace FileSharer.Common.Constants
{
    public static class ErrorMessages
    {
        public const string RequiredField = "Field is required.";

        public const string PasswordsDontMatch = "Passwords dont match.";

        public const string UserAlreadyExists = "User with such Email already exists.";

        public const string IncorrectCredentials = "Incorrect email or password.";

        public const string NoSuchCategory = "There is no such category.";

        public const string UnsupportedFormat = "Unsupported format.";

        public const string NoFileSelected = "No file selected.";
    }
}
