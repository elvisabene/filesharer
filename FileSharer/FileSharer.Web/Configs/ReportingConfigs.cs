﻿namespace FileSharer.Web.Configs
{
    public class ReportingConfigs
    {
        public int Period { get; set; }

        public string Path { get; set; }
    }
}
