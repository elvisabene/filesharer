﻿using FileSharer.Business.Services.Interfaces;
using FileSharer.Common.Constants;
using FileSharer.Common.Entities;
using FileSharer.Web.Helpers.LoggingHelpers;
using FileSharer.Web.Models.File;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileSharer.Web.Controllers
{
    [Authorize]
    public class FileController : Controller
    {
        private readonly IFileItemService _fileItemService;

        private readonly IFileCategoryService _fileCategoryService;

        private readonly IService<FileExtension> _fileExtensionService;

        private readonly IUserService _userService;

        private readonly ILogger<FileController> _logger;

        private readonly ILogHelper _logHelper;

        private readonly IFileStorageService _storageService;

        public FileController(
            IFileItemService fileItemService,
            IFileCategoryService fileCategoryService,
            IService<FileExtension> fileExtensionService,
            IUserService userService,
            ILogger<FileController> logger,
            ILogHelper logHelper,
            IFileStorageService storageService)
        {
            _fileItemService = fileItemService;
            _fileCategoryService = fileCategoryService;
            _fileExtensionService = fileExtensionService;
            _userService = userService;
            _logger = logger;
            _logHelper = logHelper;
            _storageService = storageService;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Download(int id, [FromQuery] string name)
        {
            var file = await _storageService.DownloadAsync(name);

            await _fileItemService.IncrementDownloadsCountAsync(id);

            return File(file.Data, file.ContentType, file.Name);
        }

        [HttpGet]
        public IActionResult Upload()
        {
            _logger.LogInformation(
                _logHelper.GetUserActionString(User, "File", nameof(Upload)));

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Upload(UploadFileViewModel uploadModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var category = await _fileCategoryService.GetByNameAsync(uploadModel.Category);

            if (category is null)
            {
                ModelState.AddModelError("", ErrorMessages.NoSuchCategory);

                return View();
            }

            var extension = uploadModel.File.FileName.Split('.')[^1];
            var extensionFromDatabase = (await _fileExtensionService.GetAllAsync()).
                SingleOrDefault(ext => ext.Name == "." + extension);

            if (extensionFromDatabase is null)
            {
                ModelState.AddModelError("", ErrorMessages.UnsupportedFormat);

                return View();
            }

            var file = CreateFile(uploadModel, category, extensionFromDatabase);
            await _fileItemService.AddAsync(file);

            await _storageService.UploadAsync(uploadModel.File);

            _logger.LogInformation(
                _logHelper.GetUserActionString(User, "File", nameof(Upload), "POST"));

            return RedirectToAction(nameof(List));
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var file = await _fileItemService.GetByIdAsync(id);
            var fileModel = await MapToModelAsync(file);

            _logger.LogInformation(
                _logHelper.GetUserActionString(User, "File", nameof(Details)));

            return View(fileModel);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> List()
        {
            var fileItems = await _fileItemService.GetAllAsync();
            var fileModels = new List<FileViewModel>();

            foreach (var file in fileItems)
            {
                var fileModel = await MapToModelAsync(file);
                fileModels.Add(fileModel);
            }

            ViewData["Files"] = fileModels;
            var selectList = GetSelectCategoryList();
            ViewData["Categories"] = await selectList;

            _logger.LogInformation(
                _logHelper.GetUserActionString(User, "File", nameof(List)));

            return View();
        }

        [Authorize(Roles = Roles.OnlyEditors)]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var file = await _fileItemService.GetByIdAsync(id);
            var editFileModel = await MapToEditModelAsync(file);

            _logger.LogInformation(
                _logHelper.GetUserActionString(User, "File", nameof(Edit)));

            return View(editFileModel);
        }

        [Authorize(Roles = Roles.OnlyEditors)]
        [HttpPost]
        public async Task<IActionResult> Edit(EditFileViewModel editFileModel)
        {
            if (!ModelState.IsValid)
            {
                return View(editFileModel);
            }

            var category = await _fileCategoryService.GetByNameAsync(editFileModel.NewCategory);

            if (category is null)
            {
                ModelState.AddModelError("", ErrorMessages.NoSuchCategory);

                return View(editFileModel);
            }

            var file = await MapToFileAsync(editFileModel);
            await _fileItemService.UpdateAsync(editFileModel.Id, file);

            _logger.LogInformation(
                _logHelper.GetUserActionString(User, "File", nameof(Edit), "POST"));

            return RedirectToAction(nameof(List));
        }

        [Authorize(Roles = Roles.OnlyEditors)]
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            await _fileItemService.DeleteAsync(id);

            _logger.LogInformation(
                _logHelper.GetUserActionString(User, "File", nameof(Delete), "POST"));

            return RedirectToAction(nameof(List));
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Search(SearchViewModel searchModel)
        {
            var category = await _fileCategoryService.GetByNameAsync(searchModel.Category);
            var fileName = searchModel.FileName;

            var files = await _fileItemService.GetAllAsync();

            if (category != null)
            {
                var filesByCategory = await _fileItemService.GetAllByCategoryIdAsync(category.Id);

                files = filesByCategory is null ? new List<FileItem>() : files.Where(
                    file => filesByCategory.Any(fileByCategory => fileByCategory.Id == file.Id));
            }

            if (searchModel.FileName != null)
            {
                files = files.Where(file => file.Name.Contains(fileName));
            }

            var fileModels = new List<FileViewModel>();

            foreach (var file in files)
            {
                var fileModel = await MapToModelAsync(file);
                fileModels.Add(fileModel);
            }

            ViewData["Files"] = fileModels;
            var selectList = GetSelectCategoryList();
            ViewData["Categories"] = await selectList;

            _logger.LogInformation(
                _logHelper.GetUserActionString(User, "File", nameof(Search), "POST"));

            return View(nameof(List));
        }

        private async Task<FileItem> MapToFileAsync(EditFileViewModel editModel)
        {
            var file = await _fileItemService.GetByIdAsync(editModel.Id);
            var category = await _fileCategoryService.GetByNameAsync(editModel.NewCategory);

            file.Name = editModel.NewName;
            file.FileCategoryId = category.Id;
            file.Description = editModel.NewDescription ?? string.Empty;

            return file;
        }

        private async Task<EditFileViewModel> MapToEditModelAsync(FileItem file)
        {
            var editFileModel = new EditFileViewModel()
            {
                Id = file.Id,
                NewName = file.Name,
                NewCategory = (await _fileCategoryService.GetByIdAsync(file.FileCategoryId)).Name,
                NewDescription = file.Description,
            };

            return editFileModel;
        }

        private async Task<FileViewModel> MapToModelAsync(FileItem file)
        {
            string extension = (await _fileExtensionService.GetByIdAsync(file.FileExtensionId)).Name;
            string name = file.Name + extension;
            string category = (await _fileCategoryService.GetByIdAsync(file.FileCategoryId)).Name;
            string author = (await _userService.GetByIdAsync(file.UserId)).Name;

            var fileModel = new FileViewModel()
            {
                Id = file.Id,
                Name = name,
                Category = category,
                Author = author,
                Description = file.Description,
                Size = file.Size,
                DownloadsCount = file.DownloadsCount,
                CreateDate = file.CreateDate,
            };

            return fileModel;
        }

        private FileItem CreateFile(UploadFileViewModel uploadFile, FileCategory category, FileExtension extension)
        {
            var name = uploadFile.File.FileName.Split('.')[0];
            var userId = int.Parse(User.Claims.Single(
                     claim => claim.Type == CustomClaimTypes.Id).Value);

            var file = new FileItem()
            {
                Name = name,
                FileCategoryId = category.Id,
                FileExtensionId = extension.Id,
                Description = uploadFile.Description ?? string.Empty,

                UserId = userId,
            };

            return file;
        }

        private async Task<SelectList> GetSelectCategoryList()
        {
            var categoriesList = await _fileCategoryService.GetAllAsync();

            var allCategoriesItem = new FileCategory()
            {
                Id = 0,
                Name = "All categories",
            };

            categoriesList = categoriesList.Prepend(allCategoriesItem);
            var selectList = (new SelectList(categoriesList, "Name", "Name"));

            return selectList;
        }
    }
}
