﻿using FileSharer.Business.Services.Interfaces;
using FileSharer.Common.Constants;
using FileSharer.Common.Entities;
using FileSharer.Web.Helpers.LoggingHelpers;
using FileSharer.Web.Models.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Web.Controllers
{
    [Authorize(Roles = Roles.Admin)]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        private readonly IRoleService _roleService;

        private readonly ILogger<HomeController> _logger;

        private readonly ILogHelper _logHelper;

        public UserController(
            IUserService userService,
            IRoleService roleService,
            ILogger<HomeController> logger,
            ILogHelper logHelper)
        {
            _userService = userService;
            _roleService = roleService;
            _logger = logger;
            _logHelper = logHelper;
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var users = await _userService.GetAllAsync();

            var userModels = new List<UserViewModel>();

            foreach (var user in users)
            {
                var userModel = new UserViewModel()
                {
                    Id = user.Id,
                    Name = user.Name,
                    Email = user.Email,
                    Role = (await _roleService.GetByIdAsync(user.RoleId)).Name,
                };

                userModels.Add(userModel);
            }

            _logger.LogInformation(
               _logHelper.GetUserActionString(User, "User", nameof(List)));

            return View(userModels);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await _userService.GetByIdAsync(id);
            var role = await _roleService.GetByIdAsync(user.RoleId);

            var editModel = new EditUserViewModel()
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email,
                Role = role.Name,
            };

            _logger.LogInformation(
               _logHelper.GetUserActionString(User, "User", nameof(Edit)));

            return View(editModel);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EditUserViewModel editModel)
        {
            if (!ModelState.IsValid)
            {
                return await Edit(editModel.Id);
            }

            var userByEmail = await _userService.GetByEmailAsync(editModel.Email);
            if (userByEmail != null && userByEmail.Id != editModel.Id)
            {
                ModelState.AddModelError("", ErrorMessages.UserAlreadyExists);
            }

            var role = await _roleService.GetByNameAsync(editModel.Role);

            var user = new User()
            {
                Name = editModel.Name,
                Email = editModel.Email,
                RoleId = role.Id,
            };

            await _userService.UpdateAsync(editModel.Id, user);

            _logger.LogInformation(
               _logHelper.GetUserActionString(User, "User", nameof(Edit), "POST"));

            return RedirectToAction(nameof(List));
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            await _userService.DeleteAsync(id);

            _logger.LogInformation(
               _logHelper.GetUserActionString(User, "User", nameof(Delete), "POST"));

            return RedirectToAction(nameof(List));
        }
    }
}
