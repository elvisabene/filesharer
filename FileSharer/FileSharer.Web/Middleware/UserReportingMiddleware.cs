﻿using FileSharer.Web.Configs;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileSharer.Web.Middleware
{
    public class UserReportingMiddleware
    {
        private readonly RequestDelegate next;

        private ClaimsPrincipal user;

        private bool isReporting;

        private readonly ReportingConfigs reportingConfigs;

        private CancellationTokenSource tokenSource;

        public UserReportingMiddleware(RequestDelegate next, IOptions<ReportingConfigs> reportOptions)
        {
            this.next = next;
            isReporting = false;
            reportingConfigs = reportOptions.Value;
        }

        public async Task Invoke(HttpContext context)
        {
            user = context.User;

            if (!isReporting && CanReportUser())
            {
                StartReporting();
                tokenSource = new CancellationTokenSource();
                isReporting = true;
            }

            if (isReporting && !CanReportUser())
            {
                tokenSource.Cancel();
                isReporting = false;
            }
            
            await next.Invoke(context);
        }

        private void StartReporting()
        {
            Task.Run(async () =>
            {
                while (!tokenSource.IsCancellationRequested)
                {
                    ReportUser();

                    await Task.Delay(reportingConfigs.Period * 1000);
                }
            });
        }

        private void ReportUser()
        {
            var directories = reportingConfigs.Path.Split("\\").SkipLast(1).Aggregate((x, y) => x + "\\" + y);

            if (!Directory.Exists(directories))
            {
                Directory.CreateDirectory(directories);
            }

            using (var stream = File.AppendText(reportingConfigs.Path))
            {
                stream.WriteLine($"User {GetUserInfo(user)}");
            }
        }

        private bool CanReportUser()
        {
            return user?.Identity?.IsAuthenticated ?? false;
        }

        private static string GetUserInfo(ClaimsPrincipal user)
        {
            if (!user?.Claims.Any() ?? false)
            {
                return string.Empty;
            }

            var builder = new StringBuilder();

            foreach (var claim in user.Claims)
            {
                builder.Append($"\n\t{claim.Value}");
            }

            return builder.ToString();
        }
    }
}
