﻿using FileSharer.Web.Middleware;
using Microsoft.AspNetCore.Builder;

namespace FileSharer.Web.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseReporting(this IApplicationBuilder app)
        {
            app.UseMiddleware<UserReportingMiddleware>();

            return app;
        }
    }
}
