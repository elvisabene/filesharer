﻿namespace FileSharer.Web.Models.Common
{
    public class DeleteItemModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
