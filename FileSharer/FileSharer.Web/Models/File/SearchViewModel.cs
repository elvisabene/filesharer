﻿namespace FileSharer.Web.Models.File
{
    public class SearchViewModel
    {
        public string Category { get; set; }

        public string FileName { get; set; }
    }
}
