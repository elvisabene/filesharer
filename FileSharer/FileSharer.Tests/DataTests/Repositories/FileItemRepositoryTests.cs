﻿using NUnit.Framework;
using Moq.AutoMock;
using FileSharer.Data.DataConverters;
using FileSharer.Common.Entities;
using Moq;
using FileSharer.Data.Infrastructure;
using FileSharer.Data.Repositories.Implementations;
using System.Threading.Tasks;
using System;
using FileSharer.Data.Database;

namespace FileSharer.Tests.DataTests.Repositories
{
    [TestFixture]
    public class FileItemRepositoryTests
    {
        private AutoMocker mocker;

        [SetUp]
        public void Setup()
        {
            mocker = new AutoMocker(MockBehavior.Default, DefaultValue.Mock);

            mocker.Use(new DataContext(new DatabaseSettings("connectionString")));
            mocker.Use(new FileItemConverter());
        }


        [Test]
        public void Add_WhenFileItemIsNull_ShouldThrowArgumentNullException()
        {
            // Arrange
            var dataConverter = mocker.CreateInstance<FileItemConverter>();
            var dataContext = mocker.CreateInstance<DataContext>();

            var repository = new FileItemRepository(dataContext, dataConverter);
            FileItem fileItem = null;

            //Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await repository.Add(fileItem));
        }

        [Test]
        public void Delete_WhenFileItemIdIsIncorrect_ShouldThrowArgumentOutOfRangeException()
        {
            // Arrange
            var dataConverter = mocker.CreateInstance<FileItemConverter>();
            var dataContext = mocker.CreateInstance<DataContext>();

            var repository = new FileItemRepository(dataContext, dataConverter);
            int fileItemId = -1;

            //Assert
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => await repository.Delete(fileItemId));
        }

        [Test]
        public void GetAllByCategoryId_WhenFileCategoryIdIsIncorrect_ShouldThrowArgumentOutOfRangeException()
        {
            // Arrange
            var dataConverter = mocker.CreateInstance<FileItemConverter>();
            var dataContext = mocker.CreateInstance<DataContext>();

            var repository = new FileItemRepository(dataContext, dataConverter);
            int fileCategoryId = -1;

            //Assert
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => await repository.GetAllByCategoryId(fileCategoryId));
        }

        [Test]
        public void GetAllByUserId_WhenFileUserIdIsIncorrect_ShouldThrowArgumentOutOfException()
        {
            // Arrange
            var dataConverter = mocker.CreateInstance<FileItemConverter>();
            var dataContext = mocker.CreateInstance<DataContext>();

            var repository = new FileItemRepository(dataContext, dataConverter);
            int userId = -1;

            //Assert
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => await repository.GetAllByUserId(userId));
        }

        [Test]
        public void GetAllByName_WhenFileNameIsIncorrect_ShouldThrowArgumentNullException()
        {
            // Arrange
            var dataConverter = mocker.CreateInstance<FileItemConverter>();
            var dataContext = mocker.CreateInstance<DataContext>();

            var repository = new FileItemRepository(dataContext, dataConverter);
            var fileName = string.Empty;

            //Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await repository.GetAllByName(fileName));
        }


        [Test]
        public void GetAllById_WhenIdIsIncorrect_ShouldThrowArgumentOutOfRangeException()
        {
            // Arrange
            var dataConverter = mocker.CreateInstance<FileItemConverter>();
            var dataContext = mocker.CreateInstance<DataContext>();

            var repository = new FileItemRepository(dataContext, dataConverter);
            int id = -1;

            //Assert
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => await repository.GetById(id));
        }

        [Test]
        public void Update_WhenIdIsIncorrect_ShouldThrowArgumentOutOfRangeException()
        {
            // Arrange
            var dataConverter = mocker.CreateInstance<FileItemConverter>();
            var dataContext = mocker.CreateInstance<DataContext>();

            var repository = new FileItemRepository(dataContext, dataConverter);
            var fileItem = new FileItem();
            int id = -1;

            //Assert
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => await repository.Update(id, fileItem));
        }

        [Test]
        public void Update_WhenIdFilrItemIsNull_ShouldThrowArgumentNullException()
        {
            // Arrange
            var dataConverter = mocker.CreateInstance<FileItemConverter>();
            var dataContext = mocker.CreateInstance<DataContext>();

            var repository = new FileItemRepository(dataContext, dataConverter);
            FileItem fileItem = null;
            int id = 1;

            //Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await repository.Update(id, fileItem));
        }

        [Test]
        public void IncrementDownloadsCount_WhenIdIsIncorrect_ShouldThrowArgumentOutOfRangeException()
        {
            // Arrange
            var dataConverter = mocker.CreateInstance<FileItemConverter>();
            var dataContext = mocker.CreateInstance<DataContext>();

            var repository = new FileItemRepository(dataContext, dataConverter);
            int id = -1;

            //Assert
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => await repository.IncrementDownloadsCount(id));
        }
    }
}
