﻿using FileSharer.Business.Services.Implementations;
using FileSharer.Common.Entities;
using FileSharer.Data.Repositories.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Tests.BusinessTests.Service
{
    [TestFixture]
    public class FileServiceTests
    {
        [Test]
        public async Task GetById_WhenRepositroryHasData_ShouldReturnFileItemById()
        {
            // Arrange
            var expected = new FileItem()
            {
                Id = 13,
                Name = "TextFile"
            };

            var repositoryMock = new Mock<IFileItemRepository>();
            repositoryMock.Setup(repos => repos.GetById(13)).Returns(Task.FromResult(expected));

            var fileItemService = new FileItemService(repositoryMock.Object);

            // Act
            var actual = await fileItemService.GetByIdAsync(13);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public async Task GetAll_WhenRepositoryHasData_ShouldReturnAllFileItems()
        {
            // Arrange
            IEnumerable<FileItem> expected = new List<FileItem>()
            {
                new FileItem()
                {
                    Id = 10,
                    Name = "TestFile1",
                },
                new FileItem()
                {
                    Id = 11,
                    Name = "TestFile2",
                }
            };

            var repositoryMock = new Mock<IFileItemRepository>();
            repositoryMock.Setup(repos => repos.GetAll()).Returns(Task.FromResult(expected));
            var fileService = new FileItemService(repositoryMock.Object);

            // Act
            var actual = await fileService.GetAllAsync();

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
