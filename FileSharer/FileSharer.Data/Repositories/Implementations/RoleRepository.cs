﻿using FileSharer.Common.Constants;
using FileSharer.Common.Entities;
using FileSharer.Common.Helpers;
using FileSharer.Data.DataConverters;
using FileSharer.Data.Infrastructure;
using FileSharer.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace FileSharer.Data.Repositories.Implementations
{
    public class RoleRepository : IRoleRepository
    {
        private readonly IDataConverter<Role> _converter;

        private readonly IDataContext _dataContext;

        public RoleRepository(IDataConverter<Role> converter, IDataContext dataContext)
        {
            Guard.EnsureNotNull(converter, nameof(converter));
            Guard.EnsureNotNull(dataContext, nameof(dataContext));

            _converter = converter;
            _dataContext = dataContext;
        }

        public async Task Add(Role role)
        {
            Guard.EnsureNotNull(role, nameof(role));

            string procedure = DatabaseConstants.StoredProcedureName.ForAdd.Role;
            var parameters = _converter.ConvertToSqlParameters(role);

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddRange(parameters);

            await _dataContext.ExecuteNonQueryAsync(command);
        }

        public async Task Delete(int id)
        {
            Guard.EnsurePositive(id, nameof(id));

            string procedure = DatabaseConstants.StoredProcedureName.ForDelete.Role;

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@id", id);

            await _dataContext.ExecuteNonQueryAsync(command);
        }

        public async Task<IEnumerable<Role>> GetAll()
        {
            string view = DatabaseConstants.ViewName.AllRoles;
            string query = $"SELECT * FROM {view}";

            SqlCommand command = new SqlCommand(query);
            var roles = await _dataContext.ExecuteQueryAsListAsync(command, _converter);

            return roles;
        }

        public async Task<Role> GetById(int id)
        {
            Guard.EnsurePositive(id, nameof(id));

            string view = DatabaseConstants.ViewName.AllRoles;
            string query = $"SELECT * FROM {view} " +
                           $"WHERE Id = @id";

            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@id", id);

            var role = await _dataContext.ExecuteQueryAsSingleAsync(command, _converter);

            return role;
        }

        public async Task<Role> GetByName(string name)
        {
            Guard.EnsurePresenceOfString(name, nameof(name));

            string view = DatabaseConstants.ViewName.AllRoles;
            string query = $"SELECT * FROM {view} " + $"WHERE Name = @name";
            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@name", name);
            var role = await _dataContext.ExecuteQueryAsSingleAsync(command, _converter);
             return role;
        }

        public async Task Update(int id, Role newRole)
        {
            Guard.EnsurePositive(id, nameof(id));
            Guard.EnsureNotNull(newRole, nameof(newRole));

            string procedure = DatabaseConstants.StoredProcedureName.ForUpdate.Role;
            var parameters = _converter.ConvertToSqlParameters(newRole);

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddRange(parameters);
            command.Parameters.AddWithValue("@id", id);

            await _dataContext.ExecuteNonQueryAsync(command);
        }
    }
}
