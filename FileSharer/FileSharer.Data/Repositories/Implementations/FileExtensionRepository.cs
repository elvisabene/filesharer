﻿using FileSharer.Common.Constants;
using FileSharer.Common.Entities;
using FileSharer.Common.Helpers;
using FileSharer.Data.DataConverters;
using FileSharer.Data.Infrastructure;
using FileSharer.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace FileSharer.Data.Repositories.Implementations
{
    public class FileExtensionRepository : IRepository<FileExtension>
    {
        public IDataContext _dataContext;

        public IDataConverter<FileExtension> _converter;

        public FileExtensionRepository(IDataContext dataContext, IDataConverter<FileExtension> converter)
        {
            Guard.EnsureNotNull(dataContext, nameof(dataContext));
            Guard.EnsureNotNull(converter, nameof(converter));

            _dataContext = dataContext;
            _converter = converter;
        }

        public async Task Add(FileExtension item)
        {
            Guard.EnsureNotNull(item, nameof(item));

            var parameters = _converter.ConvertToSqlParameters(item);
            var procedure = DatabaseConstants.StoredProcedureName.ForAdd.FileExtension;

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddRange(parameters);

            await _dataContext.ExecuteNonQueryAsync(command);
        }

        public async Task Delete(int id)
        {
            Guard.EnsurePositive(id, nameof(id));

            var procedure = DatabaseConstants.StoredProcedureName.ForDelete.FileExtension;

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@id", id);

            await _dataContext.ExecuteNonQueryAsync(command);
        }

        public async Task<IEnumerable<FileExtension>> GetAll()
        {
            var view = DatabaseConstants.ViewName.AllFileExtensions;
            var query = $"SELECT * FROM {view}";

            SqlCommand command = new SqlCommand(query);

            var fileExtensions = await _dataContext.ExecuteQueryAsListAsync(command, _converter);

            return fileExtensions;
        }

        public async Task<FileExtension> GetById(int id)
        {
            Guard.EnsurePositive(id, nameof(id));

            var view = DatabaseConstants.ViewName.AllFileExtensions;
            var query = $"SELECT * FROM {view} " +
                        $"WHERE Id = @id";

            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@id", id);

            var fileExtension = await _dataContext.ExecuteQueryAsSingleAsync(command, _converter);

            return fileExtension;
        }

        public async Task Update(int id, FileExtension newItem)
        {
            Guard.EnsurePositive(id, nameof(id));
            Guard.EnsureNotNull(newItem, nameof(newItem));

            var procedure = DatabaseConstants.StoredProcedureName.ForUpdate.FileExtension;
            var parameters = _converter.ConvertToSqlParameters(newItem);

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddRange(parameters);
            command.Parameters.AddWithValue("@id", id);

            await _dataContext.ExecuteNonQueryAsync(command);
        }
    }
}
