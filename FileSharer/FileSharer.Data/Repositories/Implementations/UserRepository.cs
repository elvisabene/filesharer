using FileSharer.Common.Constants;
using FileSharer.Common.Entities;
using FileSharer.Common.Helpers;
using FileSharer.Data.DataConverters;
using FileSharer.Data.Infrastructure;
using FileSharer.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace FileSharer.Data.Repositories.Implementations
{
    public class UserRepository : IUserRepository
    {
        private IDataContext _dataContext;

        private IDataConverter<User> _converter;

        public UserRepository(IDataContext dataContext, IDataConverter<User> converter)
        {
            Guard.EnsureNotNull(dataContext, nameof(dataContext));
            Guard.EnsureNotNull(converter, nameof(converter));

            _dataContext = dataContext;
            _converter = converter;
        }

        public async Task Add(User user)
        {
            Guard.EnsureNotNull(user, nameof(user));

            string procedure = DatabaseConstants.StoredProcedureName.ForAdd.User;
            SqlParameter[] parameters = _converter.ConvertToSqlParameters(user);

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddRange(parameters);

            await _dataContext.ExecuteNonQueryAsync(command);
        }

        public async Task Delete(int id)
        {
            Guard.EnsurePositive(id, nameof(id));

            string procedure = DatabaseConstants.StoredProcedureName.ForDelete.User;

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@id", id);

            await _dataContext.ExecuteNonQueryAsync(command);
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            string view = DatabaseConstants.ViewName.AllUsers;
            string query = $"SELECT * FROM {view}";

            SqlCommand command = new SqlCommand(query);

            var users = await _dataContext.ExecuteQueryAsListAsync(command, _converter);

            return users;
        }

        public async Task<IEnumerable<User>> GetAllByName(string name)
        {
            Guard.EnsurePresenceOfString(name, nameof(name));

            string view = DatabaseConstants.ViewName.AllUsers;
            string query = $"SELECT * FROM {view} " +
                           $"WHERE Name = @name";

            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@name", name);

            var users = await _dataContext.ExecuteQueryAsListAsync(command, _converter);

            return users;
        }

        public async Task<User> GetByEmail(string email)
        {
            Guard.EnsurePresenceOfString(email, nameof(email));

            string view = DatabaseConstants.ViewName.AllUsers;
            string query = $"SELECT * FROM {view} " +
                           $"WHERE Email = @email";

            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@email", email);

            var user = await _dataContext.ExecuteQueryAsSingleAsync(command, _converter);

            return user;
        }

        public async Task<User> GetById(int id)
        {
            Guard.EnsurePositive(id, nameof(id));

            string view = DatabaseConstants.ViewName.AllUsers;
            string query = $"SELECT * FROM {view} " +
                           $"WHERE Id = @id";

            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@id", id);

            var user = await _dataContext.ExecuteQueryAsSingleAsync(command, _converter);

            return user;
        }

        public async Task Update(int id, User newUser)
        {
            Guard.EnsurePositive(id, nameof(id));
            Guard.EnsureNotNull(newUser, nameof(newUser));

            string procedure = DatabaseConstants.StoredProcedureName.ForUpdate.User;
            SqlParameter[] parameters = _converter.ConvertToSqlParameters(newUser);

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddRange(parameters);

            await _dataContext.ExecuteNonQueryAsync(command);
        }
    }
}
