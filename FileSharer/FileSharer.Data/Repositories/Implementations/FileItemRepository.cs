﻿using FileSharer.Common.Constants;
using FileSharer.Common.Entities;
using FileSharer.Common.Helpers;
using FileSharer.Data.DataConverters;
using FileSharer.Data.Infrastructure;
using FileSharer.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace FileSharer.Data.Repositories.Implementations
{
    public class FileItemRepository : IFileItemRepository
    {
        private readonly IDataContext _dataContext;

        private readonly IDataConverter<FileItem> _converter;

        public FileItemRepository(IDataContext dataContext, IDataConverter<FileItem> converter)
        {
            Guard.EnsureNotNull(dataContext, nameof(dataContext));
            Guard.EnsureNotNull(converter, nameof(converter));

            _dataContext = dataContext;
            _converter = converter;
        }

        public async Task Add(FileItem fileItem)
        {
            Guard.EnsureNotNull(fileItem, nameof(fileItem));

            string procedure = DatabaseConstants.StoredProcedureName.ForAdd.FileItem;
            SqlParameter[] parameters = _converter.ConvertToSqlParameters(fileItem);

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddRange(parameters);

            await _dataContext.ExecuteNonQueryAsync(command);
        }

        public async Task Delete(int id)
        {
            Guard.EnsurePositive(id, nameof(id));

            string procedure = DatabaseConstants.StoredProcedureName.ForDelete.FileItem;

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@id", id);

            await _dataContext.ExecuteNonQueryAsync(command);
        }

        public async Task<IEnumerable<FileItem>> GetAll()
        {
            string view = DatabaseConstants.ViewName.AllFileItems;
            string query = $"SELECT * FROM {view}";

            SqlCommand command = new SqlCommand(query);

            var fileItems = await _dataContext.ExecuteQueryAsListAsync(command, _converter);

            return fileItems;
        }

        public async Task<IEnumerable<FileItem>> GetAllByCategoryId(int categoryId)
        {
            Guard.EnsurePositive(categoryId, nameof(categoryId));

            string view = DatabaseConstants.ViewName.AllFileItems;
            string query = $"SELECT * FROM {view} " +
                           $"WHERE CategoryId = @categoryId";

            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@categoryId", categoryId);
            
            var fileItems = await _dataContext.ExecuteQueryAsListAsync(command, _converter);

            return fileItems;
        }

        public async Task<IEnumerable<FileItem>> GetAllByUserId(int userId)
        {
            Guard.EnsurePositive(userId, nameof(userId));

            string view = DatabaseConstants.ViewName.AllFileItems;
            string query = $"SELECT * FROM {view} " +
                           $"WHERE UserId = @userId";

            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@userId", userId);

            var fileItems = await _dataContext.ExecuteQueryAsListAsync(command, _converter);

            return fileItems;
        }

        public async Task<IEnumerable<FileItem>> GetAllByName(string name)
        {
            Guard.EnsurePresenceOfString(name, nameof(name));

            string view = DatabaseConstants.ViewName.AllFileItems;
            string query = $"SELECT * FROM {view} " +
                           $"WHERE Name = @name";

            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@name", name);

            var fileItems = await _dataContext.ExecuteQueryAsListAsync(command, _converter);

            return fileItems;
        }

        public async Task<FileItem> GetById(int id)
        {
            Guard.EnsurePositive(id, nameof(id));

            string view = DatabaseConstants.ViewName.AllFileItems;
            string query = $"SELECT * FROM {view} " +
                           $"WHERE Id = @id";

            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@id", id);

            var fileItem = await _dataContext.ExecuteQueryAsSingleAsync(command, _converter);

            return fileItem;
        }

        public async Task Update(int id, FileItem newFileItem)
        {
            Guard.EnsurePositive(id, nameof(id));
            Guard.EnsureNotNull(newFileItem, nameof(newFileItem));

            string procedure = DatabaseConstants.StoredProcedureName.ForUpdate.FileItem;
            SqlParameter[] parameters = _converter.ConvertToSqlParameters(newFileItem);

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddRange(parameters);

            await _dataContext.ExecuteNonQueryAsync(command);
        }

        public async Task IncrementDownloadsCount(int id)
        {
            Guard.EnsurePositive(id, nameof(id));

            string procedure = DatabaseConstants.StoredProcedureName.ForUpdate.DownloadsCount;

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@id", id);

            await _dataContext.ExecuteNonQueryAsync(command);
        }
    }
}
