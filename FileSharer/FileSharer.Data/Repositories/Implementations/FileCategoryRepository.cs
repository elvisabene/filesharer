﻿using FileSharer.Common.Constants;
using FileSharer.Common.Entities;
using FileSharer.Common.Helpers;
using FileSharer.Data.DataConverters;
using FileSharer.Data.Infrastructure;
using FileSharer.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace FileSharer.Data.Repositories.Implementations
{
    public class FileCategoryRepository : IFileCategoryRepository
    {
        public IDataContext _dataContext;

        public IDataConverter<FileCategory> _converter;

        public FileCategoryRepository(IDataContext dataContext, IDataConverter<FileCategory> converter)
        {
            Guard.EnsureNotNull(dataContext, nameof(dataContext));
            Guard.EnsureNotNull(converter, nameof(converter));

            _dataContext = dataContext;
            _converter = converter;
        }

        public async Task Add(FileCategory item)
        {
            Guard.EnsureNotNull(item, nameof(item));

            var parameters = _converter.ConvertToSqlParameters(item);
            var procedure = DatabaseConstants.StoredProcedureName.ForAdd.FileCategory;

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddRange(parameters);

            await _dataContext.ExecuteNonQueryAsync(command);
        }

        public async Task Delete(int id)
        {
            Guard.EnsurePositive(id, nameof(id));

            var procedure = DatabaseConstants.StoredProcedureName.ForDelete.FileCategory;

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@id", id);

            await _dataContext.ExecuteNonQueryAsync(command);
        }

        public async Task<IEnumerable<FileCategory>> GetAll()
        {
            var view = DatabaseConstants.ViewName.AllFileCategories;
            var query = $"SELECT * FROM {view}";

            SqlCommand command = new SqlCommand(query);

            var fileCategories = await _dataContext.ExecuteQueryAsListAsync(command, _converter);

            return fileCategories;
        }

        public async Task<FileCategory> GetById(int id)
        {
            Guard.EnsurePositive(id, nameof(id));

            var view = DatabaseConstants.ViewName.AllFileCategories;
            var query = $"SELECT * FROM {view} " +
                        $"WHERE Id = @id";

            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@id", id);

            var fileCategory = await _dataContext.ExecuteQueryAsSingleAsync(command, _converter);

            return fileCategory;
        }

        public async Task<FileCategory> GetByName(string name)
        {
            Guard.EnsurePresenceOfString(name, nameof(name));

            var view = DatabaseConstants.ViewName.AllFileCategories;
            var query = $"SELECT * FROM {view} " + $"WHERE Name = @name";
            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@name", name);
            var fileCategory = await _dataContext.ExecuteQueryAsSingleAsync(command, _converter);

            return fileCategory;
        }

        public async Task Update(int id, FileCategory newItem)
        {
            Guard.EnsurePositive(id, nameof(id));
            Guard.EnsureNotNull(newItem, nameof(newItem));

            var procedure = DatabaseConstants.StoredProcedureName.ForUpdate.FileCategory;
            var parameters = _converter.ConvertToSqlParameters(newItem);

            SqlCommand command = new SqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddRange(parameters);
            command.Parameters.AddWithValue("@id", id);

            await _dataContext.ExecuteNonQueryAsync(command);
        }
    }
}
