﻿using FileSharer.Common.Entities;
using System.Threading.Tasks;

namespace FileSharer.Data.Repositories.Interfaces
{
    public interface IRoleRepository : IRepository<Role>
    {
        Task<Role> GetByName(string name);
    }
}
