﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Data.Repositories.Interfaces
{
    public interface IRepository<T>
    {
        Task Add(T item);

        Task<IEnumerable<T>> GetAll();

        Task<T> GetById(int id);

        Task Update(int id, T newItem);

        Task Delete(int id);
    }
}
