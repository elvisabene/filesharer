using FileSharer.Common.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Data.Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetByEmail(string email);

        Task<IEnumerable<User>> GetAllByName(string name);
    }
}
