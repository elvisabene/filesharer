﻿using FileSharer.Common.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Data.Repositories.Interfaces
{
    public interface IFileItemRepository : IRepository<FileItem>
    {
        Task<IEnumerable<FileItem>> GetAllByUserId(int userId);

        Task<IEnumerable<FileItem>> GetAllByCategoryId(int categoryId);

        Task<IEnumerable<FileItem>> GetAllByName(string name);

        Task IncrementDownloadsCount(int id);
    }
}
