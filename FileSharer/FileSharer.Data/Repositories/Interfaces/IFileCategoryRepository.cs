﻿using FileSharer.Common.Entities;
using System.Threading.Tasks;

namespace FileSharer.Data.Repositories.Interfaces
{
    public interface IFileCategoryRepository : IRepository<FileCategory>
    {
        public Task<FileCategory> GetByName(string name);
    }
}
