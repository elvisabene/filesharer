﻿using FileSharer.Data.DataConverters;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace FileSharer.Data.Infrastructure
{
    public interface IDataContext
    {
        Task ExecuteNonQueryAsync(SqlCommand command);

        Task<T> ExecuteQueryAsSingleAsync<T>(SqlCommand command, IDataConverter<T> converter);

        Task<IEnumerable<T>> ExecuteQueryAsListAsync<T>(SqlCommand command, IDataConverter<T> converter);
    }
}
