﻿using FileSharer.Common.Helpers;
using FileSharer.Data.Database;
using FileSharer.Data.DataConverters;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace FileSharer.Data.Infrastructure
{
    public class DataContext : IDataContext
    {
        private readonly IDatabaseSettings _dbSettings;

        public DataContext(IDatabaseSettings dbSettings)
        {
            Guard.EnsureNotNull(dbSettings, nameof(dbSettings));
            _dbSettings = dbSettings;
        }

        public async Task ExecuteNonQueryAsync(SqlCommand command)
        {
            Guard.EnsureNotNull(command, nameof(command));

            using (SqlConnection connection = new SqlConnection(_dbSettings.ConnectionString))
            {
                command.Connection = connection;
                connection.Open();
                await command.ExecuteNonQueryAsync();
            }
        }

        public async Task<T> ExecuteQueryAsSingleAsync<T>(SqlCommand command, IDataConverter<T> converter)
        {
            Guard.EnsureNotNull(command, nameof(command));
            Guard.EnsureNotNull(converter, nameof(converter));

            using (SqlConnection connection = new SqlConnection(_dbSettings.ConnectionString))
            {
                command.Connection = connection;
                connection.Open();
                SqlDataReader reader = await command.ExecuteReaderAsync();

                T item = converter.ConvertToSingleItem(reader);

                return item;
            }
        }

        public async Task<IEnumerable<T>> ExecuteQueryAsListAsync<T>(SqlCommand command, IDataConverter<T> converter)
        {
            Guard.EnsureNotNull(command, nameof(command));
            Guard.EnsureNotNull(converter, nameof(converter));

            using (SqlConnection connection = new SqlConnection(_dbSettings.ConnectionString))
            {
                command.Connection = connection;
                connection.Open();
                SqlDataReader reader = await command.ExecuteReaderAsync();

                IEnumerable<T> items = converter.ConvertToItemList(reader);

                return items;
            }
        }
    }
}
