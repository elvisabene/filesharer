﻿namespace FileSharer.Data.Database
{
    public interface IDatabaseSettings
    {
        string ConnectionString { get; }
    }
}
