﻿using FileSharer.Common.Helpers;
using System;

namespace FileSharer.Data.Database
{
    public class DatabaseSettings : IDatabaseSettings
    {
        public string ConnectionString { get; }

        public DatabaseSettings(string connectionString)
        {
            Guard.EnsurePresenceOfString(connectionString, nameof(connectionString));
            ConnectionString = connectionString;
        }
    }
}
