﻿using FileSharer.Common.Entities;
using FileSharer.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace FileSharer.Data.DataConverters
{
    public class RoleConverter : IDataConverter<Role>
    {
        public IEnumerable<Role> ConvertToItemList(SqlDataReader reader)
        {
            Guard.EnsureNotNull(reader, nameof(reader));

            var roles = new List<Role>();

            if (!reader.HasRows)
            {
                return roles;
            }


            while (reader.Read())
            {
                var role = ConvertToSingleItem(reader, false);
                roles.Add(role);
            }

            return roles;
        }

        public Role ConvertToSingleItem(SqlDataReader reader, bool withRead = true)
        {
            Guard.EnsureNotNull(reader, nameof(reader));

            if (!reader.HasRows)
            {
                return null;
            }

            if (withRead)
            {
                reader.Read();
            }

            var role = new Role()
            {
                Id = (int)reader["Id"],
                Name = (string)reader["Name"],
            };

            return role;
        }

        public SqlParameter[] ConvertToSqlParameters(Role role)
        {
            Guard.EnsureNotNull(role, nameof(role));

            SqlParameter[] parameters =
            {
                new SqlParameter()
                {
                    ParameterName = "@name",
                    DbType = DbType.String,
                    Value = role.Name,
                }
            };

            return parameters;
        }
    }
}
