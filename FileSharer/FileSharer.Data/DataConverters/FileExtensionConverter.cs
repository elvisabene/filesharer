﻿using FileSharer.Common.Entities;
using FileSharer.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace FileSharer.Data.DataConverters
{
    public class FileExtensionConverter : IDataConverter<FileExtension>
    {
        public IEnumerable<FileExtension> ConvertToItemList(SqlDataReader reader)
        {
            Guard.EnsureNotNull(reader, nameof(reader));

            var fileCategories = new List<FileExtension>();

            if (!reader.HasRows)
            {
                return fileCategories;
            }

            while (reader.Read())
            {
                var fileCategory = ConvertToSingleItem(reader, false);
                fileCategories.Add(fileCategory);
            }

            return fileCategories;
        }

        public FileExtension ConvertToSingleItem(SqlDataReader reader, bool withRead = true)
        {
            Guard.EnsureNotNull(reader, nameof(reader));

            if (!reader.HasRows)
            {
                return null;
            }

            if (withRead)
            {
                reader.Read();
            }

            var fileCategory = new FileExtension()
            {
                Id = (int)reader["Id"],
                Name = (string)reader["Name"],
            };

            return fileCategory;
        }

        public SqlParameter[] ConvertToSqlParameters(FileExtension fileExtension)
        {
            Guard.EnsureNotNull(fileExtension, nameof(fileExtension));

            SqlParameter[] parameters =
            {
                new SqlParameter()
                {
                    ParameterName = "@name",
                    DbType = DbType.String,
                    Value = fileExtension.Name,
                }
            };

            return parameters;
        }
    }
}
