﻿using FileSharer.Common.Entities;
using FileSharer.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace FileSharer.Data.DataConverters
{
    public class FileCategoryConverter : IDataConverter<FileCategory>
    {
        public IEnumerable<FileCategory> ConvertToItemList(SqlDataReader reader)
        {
            Guard.EnsureNotNull(reader, nameof(reader));

            var fileCategories = new List<FileCategory>();

            if (!reader.HasRows)
            {
                return fileCategories;
            }

            while (reader.Read())
            {
                var fileCategory = ConvertToSingleItem(reader, false);
                fileCategories.Add(fileCategory);
            }

            return fileCategories;
        }

        public FileCategory ConvertToSingleItem(SqlDataReader reader, bool withRead = true)
        {
            Guard.EnsureNotNull(reader, nameof(reader));

            if (!reader.HasRows)
            {
                return null;
            }

            if (withRead)
            {
                reader.Read();
            }

            var fileCategory = new FileCategory()
            {
                Id = (int)reader["Id"],
                Name = (string)reader["Name"],
            };

            return fileCategory;
        }

        public SqlParameter[] ConvertToSqlParameters(FileCategory fileCategory)
        {
            Guard.EnsureNotNull(fileCategory, nameof(fileCategory));

            if (fileCategory is null)
            {
                throw new ArgumentNullException(nameof(fileCategory));
            }

            SqlParameter[] parameters =
            {
                new SqlParameter()
                {
                    ParameterName = "@name",
                    DbType = DbType.String,
                    Value = fileCategory.Name,
                }
            };

            return parameters;
        }
    }
}
