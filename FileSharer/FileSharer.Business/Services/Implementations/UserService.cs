﻿using FileSharer.Business.Services.Interfaces;
using FileSharer.Common.Entities;
using FileSharer.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task AddAsync(User user)
        {
            await _userRepository.Add(user);
        }

        public async Task DeleteAsync(int id)
        {
            await _userRepository.Delete(id);
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            var users = await _userRepository.GetAll();

            return users;
        }

        public async Task<IEnumerable<User>> GetAllByNameAsync(string name)
        {
            var users = await _userRepository.GetAllByName(name);

            return users;
        }

        public async Task<User> GetByEmailAsync(string email)
        {
            var user = await _userRepository.GetByEmail(email);

            return user;
        }

        public async Task<User> GetByIdAsync(int id)
        {
            var user = await _userRepository.GetById(id);

            return user;
        }

        public async Task UpdateAsync(int id, User newUser)
        {
            newUser.PasswordHash = (await _userRepository.GetById(id)).PasswordHash;
            await _userRepository.Update(id, newUser);
        }
    }
}
