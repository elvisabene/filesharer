﻿using FileSharer.Business.Services.Interfaces;
using FileSharer.Common.Constants;
using FileSharer.Common.Entities;
using FileSharer.Common.Extensions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Implementations
{
    public class AccountService : IAccountService
    {
        private readonly IUserService _userService;

        private readonly IRoleService _roleService;

        public AccountService(IUserService userService, IRoleService roleService)
        {
            _userService = userService;
            _roleService = roleService;
        }

        public async Task<bool> IsUserExistsAsync(string email)
        {
            var user = await _userService.GetByEmailAsync(email);

            return user != null;
        }

        public async Task<bool> IsUserExistsAsync(string email, string password)
        {
            User user = null;

            if (!await IsUserExistsAsync(email))
            {
                return false;
            }

            user = await _userService.GetByEmailAsync(email);
            var passwordHash = password.GetHashSHA256();

            return user.PasswordHash == passwordHash;
        }

        public async Task Authenticate(User user, HttpContext httpContext)
        {
            if (user is null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var role = await _roleService.GetByIdAsync(user.RoleId);

            var claims = new List<Claim>()
            {
                new Claim(CustomClaimTypes.Id, user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.Role, role.Name),
            };

            var identity = new ClaimsIdentity(claims, "AuthenticationCookie");

            var principal = new ClaimsPrincipal(identity);

            await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
        }

        public async Task<User> CreateUserAsync(string name, string email, string password)
        {
            var passwordHash = password.GetHashSHA256();

            var newUser = new User()
            {
                Name = name,
                Email = email,
                RoleId = Defaults.RoleId,
                PasswordHash = passwordHash,
            };

            await _userService.AddAsync(newUser);

            return newUser;
        }
    }
}
