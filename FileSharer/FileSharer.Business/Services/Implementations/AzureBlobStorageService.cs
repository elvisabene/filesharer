﻿using Azure.Storage.Blobs;
using FileSharer.Business.Services.Interfaces;
using FileSharer.Common.Entities;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Implementations
{
    public class AzureBlobStorageService : IFileStorageService
    {
        private readonly BlobServiceClient _blobService;

        public AzureBlobStorageService(BlobServiceClient blobService)
        {
            _blobService = blobService;
        }

        public async Task<PhysicalFile> DownloadAsync(string fileName)
        {
            var blobContainer = _blobService.GetBlobContainerClient("files");
            var blob = blobContainer.GetBlobClient(fileName);

            var contentType = blob.GetProperties().Value.ContentType;

            var data = (await blob.DownloadContentAsync()).Value.Content.ToArray();

            var file = new PhysicalFile
            {
                Data = data,
                Name = fileName,
                ContentType = contentType,
            };

            return file;
        }

        public async Task UploadAsync(IFormFile file)
        {
            var blobContainer = _blobService.GetBlobContainerClient("files");

            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                ms.Position = 0;
                await blobContainer.UploadBlobAsync(file.FileName, ms);
            }
        }
    }
}
