﻿using FileSharer.Business.Services.Interfaces;
using FileSharer.Common.Entities;
using FileSharer.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Implementations
{
    public class FileCategoryService : IFileCategoryService
    {
        private IFileCategoryRepository _fileCategoryRepository;

        public FileCategoryService(IFileCategoryRepository fileCategoryRepository)
        {
            _fileCategoryRepository = fileCategoryRepository;
        }

        public async Task AddAsync(FileCategory item)
        {
            await _fileCategoryRepository.Add(item);
        }

        public async Task DeleteAsync(int id)
        {
            await _fileCategoryRepository.Delete(id);
        }

        public async Task<IEnumerable<FileCategory>> GetAllAsync()
        {
            var fileExtensions = await _fileCategoryRepository.GetAll();

            return fileExtensions;
        }

        public async Task<FileCategory> GetByIdAsync(int id)
        {
            var fileCategory = await _fileCategoryRepository.GetById(id);

            return fileCategory;
        }

        public async Task<FileCategory> GetByNameAsync(string name)
        {
            var fileCategory = await _fileCategoryRepository.GetByName(name);

            return fileCategory;
        }

        public async Task UpdateAsync(int id, FileCategory newItem)
        {
            await _fileCategoryRepository.Update(id, newItem);
        }
    }
}
