﻿using FileSharer.Business.Services.Interfaces;
using FileSharer.Common.Entities;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Mime;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Implementations
{
    public class MockFileStorageService : IFileStorageService
    {
        public async Task<PhysicalFile> DownloadAsync(string fileName)
        {
            fileName = "file.txt";
            var physicalFile = new PhysicalFile();

            using (var fileStream = File.Create(fileName))
            {
                var writer = new StreamWriter(fileStream);
                writer.WriteLine("This is sample file!");
                writer.Flush();
            }

            physicalFile.Data = await File.ReadAllBytesAsync(fileName);
            physicalFile.Name = fileName;
            physicalFile.ContentType = MediaTypeNames.Text.Plain;

            File.Delete(fileName);

            return physicalFile;
        }

        public async Task UploadAsync(IFormFile file)
        {
            await Task.CompletedTask;
        }
    }
}
