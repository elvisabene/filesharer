﻿using FileSharer.Business.Services.Interfaces;
using FileSharer.Common.Entities;
using FileSharer.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Implementations
{
    public class FileItemService : IFileItemService
    {
        private readonly IFileItemRepository _fileItemRepository;

        public FileItemService(IFileItemRepository fileItemRepository)
        {
            _fileItemRepository = fileItemRepository;
        }

        public async Task AddAsync(FileItem fileItem)
        {
            await _fileItemRepository.Add(fileItem);
        }

        public async Task DeleteAsync(int id)
        {
            await _fileItemRepository.Delete(id);
        }

        public async Task<IEnumerable<FileItem>> GetAllAsync()
        {
            var files = await _fileItemRepository.GetAll();

            return files;
        }

        public async Task<IEnumerable<FileItem>> GetAllByCategoryIdAsync(int categoryId)
        {
            var files = await _fileItemRepository.GetAllByCategoryId(categoryId);

            return files;
        }

        public async Task<IEnumerable<FileItem>> GetAllByUserIdAsync(int userId)
        {
            var files = await _fileItemRepository.GetAllByUserId(userId);
             return files;
        }

        public async Task<IEnumerable<FileItem>> GetAllByNameAsync(string name)
        {
            var files = await _fileItemRepository.GetAllByName(name);
            return files;
        }

        public async Task<FileItem> GetByIdAsync(int id)
        {
            var file = await _fileItemRepository.GetById(id);

            return file;
        }

        public async Task UpdateAsync(int id, FileItem newFileItem)
        {
            await _fileItemRepository.Update(id, newFileItem);
        }

        public async Task IncrementDownloadsCountAsync(int id)
        {
            await _fileItemRepository.IncrementDownloadsCount(id);
        }
    }
}
