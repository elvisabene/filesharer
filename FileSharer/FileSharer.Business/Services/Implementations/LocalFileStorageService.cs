﻿using FileSharer.Business.Services.Interfaces;
using FileSharer.Common.Entities;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Implementations
{
    public class LocalFileStorageService : IFileStorageService
    {
        public Task<PhysicalFile> DownloadAsync(string fileName)
        {
            throw new System.NotImplementedException();
        }

        public Task UploadAsync(IFormFile file)
        {
            throw new System.NotImplementedException();
        }
    }
}
