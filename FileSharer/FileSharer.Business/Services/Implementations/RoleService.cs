﻿using FileSharer.Business.Services.Interfaces;
using FileSharer.Common.Entities;
using FileSharer.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Implementations
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;

        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public async Task AddAsync(Role role)
        {
            await _roleRepository.Add(role);
        }

        public async Task DeleteAsync(int id)
        {
            await _roleRepository.Delete(id);
        }

        public async Task<IEnumerable<Role>> GetAllAsync()
        {
            var roles = await _roleRepository.GetAll();

            return roles;
        }

        public async Task<Role> GetByIdAsync(int id)
        {
            var role = await _roleRepository.GetById(id);

            return role;
        }

        public async Task<Role> GetByNameAsync(string name)
        {
            var role = await _roleRepository.GetByName(name);

            return role;
        }

        public async Task UpdateAsync(int id, Role newRole)
        {
            await _roleRepository.Update(id, newRole);
        }
    }
}
