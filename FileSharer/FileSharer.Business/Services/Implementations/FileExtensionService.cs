﻿using FileSharer.Business.Services.Interfaces;
using FileSharer.Common.Entities;
using FileSharer.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Implementations
{
    public class FileExtensionService : IService<FileExtension>
    {
        private IRepository<FileExtension> _fileExtensionRepository;

        public FileExtensionService(IRepository<FileExtension> fileExtensionRepository)
        {
            _fileExtensionRepository = fileExtensionRepository;
        }

        public async Task AddAsync(FileExtension item)
        {
            await _fileExtensionRepository.Add(item);
        }

        public async Task DeleteAsync(int id)
        {
            await _fileExtensionRepository.Delete(id);
        }

        public async Task<IEnumerable<FileExtension>> GetAllAsync()
        {
            var fileExtensions = await _fileExtensionRepository.GetAll();

            return fileExtensions;
        }

        public async Task<FileExtension> GetByIdAsync(int id)
        {
            var fileExtension = await _fileExtensionRepository.GetById(id);

            return fileExtension;
        }

        public async Task UpdateAsync(int id, FileExtension newItem)
        {
            await _fileExtensionRepository.Update(id, newItem);
        }
    }
}
