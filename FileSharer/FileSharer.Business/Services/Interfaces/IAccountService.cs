﻿using FileSharer.Common.Entities;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Interfaces
{
    public interface IAccountService
    {
        Task<User> CreateUserAsync(string name, string email, string password);

        Task Authenticate(User user, HttpContext httpContext);

        Task<bool> IsUserExistsAsync(string email);

        Task<bool> IsUserExistsAsync(string email, string password);
    }
}
