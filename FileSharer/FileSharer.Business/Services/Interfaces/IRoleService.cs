﻿using FileSharer.Common.Entities;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Interfaces
{
    public interface IRoleService : IService<Role>
    {
        Task<Role> GetByNameAsync(string name);
    }
}
