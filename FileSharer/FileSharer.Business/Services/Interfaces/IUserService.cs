﻿using FileSharer.Common.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Interfaces
{
    public interface IUserService : IService<User>
    {
        Task<User> GetByEmailAsync(string email);

        Task<IEnumerable<User>> GetAllByNameAsync(string name);
    }
}
