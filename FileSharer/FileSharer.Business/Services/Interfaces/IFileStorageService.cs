﻿using FileSharer.Common.Entities;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Interfaces
{
    public interface IFileStorageService
    {
        Task UploadAsync(IFormFile file);

        Task<PhysicalFile> DownloadAsync(string fileName);
    }
}
