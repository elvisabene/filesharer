﻿using FileSharer.Common.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Interfaces
{
    public interface IFileItemService : IService<FileItem>
    {
        Task<IEnumerable<FileItem>> GetAllByUserIdAsync(int userId);

        Task<IEnumerable<FileItem>> GetAllByCategoryIdAsync(int categoryId);

        Task<IEnumerable<FileItem>> GetAllByNameAsync(string name);

        Task IncrementDownloadsCountAsync(int id);
    }
}
