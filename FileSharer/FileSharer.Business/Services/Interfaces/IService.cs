﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Interfaces
{
    public interface IService<T>
    {
        Task AddAsync(T item);

        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(int id);

        Task UpdateAsync(int id, T newItem);

        Task DeleteAsync(int id);
    }
}
