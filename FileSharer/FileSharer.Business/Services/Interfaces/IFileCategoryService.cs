﻿using FileSharer.Common.Entities;
using System.Threading.Tasks;

namespace FileSharer.Business.Services.Interfaces
{
    public interface IFileCategoryService : IService<FileCategory>
    {
        Task<FileCategory> GetByNameAsync(string name);
    }
}
