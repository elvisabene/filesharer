CREATE VIEW dbo.vRole AS
SELECT r.Id AS Id,
r.Name AS Name
FROM dbo.tRole AS r;
