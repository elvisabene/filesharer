INSERT INTO dbo.tFileItem
(Name, FileExtensionId, Size, UserId, FileCategoryId)
VALUES
('MyDoc', 6, 5000, 1, 2),
('SomeImage', 1, 5000, 1, 1),
('SomeVideo', 11, 5000, 2, 3),
('SomePresentation', 8, 5000, 2, 2),
('MyZip', 15, 5000, 3, 5),
('MyText', 13, 5000, 3, 6);
