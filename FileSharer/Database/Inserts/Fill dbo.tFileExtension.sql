INSERT INTO dbo.tFileExtension
VALUES
('.jpg'), ('.jpeg'), ('.png'), ('.gif'),
('.pdf'), ('.doc'), ('.xls'), ('.ppt'),
('.mp3'), ('.wav'), ('.mp4'), ('.avi'),
('.txt'), ('.rar'), ('.zip'), ('.7z');
