CREATE PROCEDURE dbo.spUpdateUser
    @id INT,
    @name NVARCHAR(50),
    @roleId INT,
    @email NVARCHAR(50),
    @passwordHash NVARCHAR(100)
AS
BEGIN
    IF @id IS NULL OR @name IS NULL
        OR NOT EXISTS(SELECT * FROM dbo.tRole WHERE Id = @roleId)
        OR @roleId IS NULL OR @email IS NULL OR @passwordHash IS NULL
        THROW 50000, 'Arguments was null or not existing!', 1
    ELSE
        UPDATE dbo.tUser
        SET Name = @name,
            RoleId = @roleId,
            Email = @email,
            PasswordHash = @passwordHash
        WHERE Id = @id
END
